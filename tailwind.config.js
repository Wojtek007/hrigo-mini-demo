module.exports = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
    "./modules/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      backdropBlur: {
        xs: '2px',
      },
      boxShadow: {
        'blur-edges': '0 0 5px 5px #fff',
      },
      colors: {
        'copper': {
          DEFAULT: "#995B4D",
          50: "#EEE0DD",
          100: "#E5D1CC",
          200: "#D4B2AA",
          300: "#C39388",
          400: "#B37566",
          500: "#995B4D",
          600: "#77473C",
          700: "#55332B",
          800: "#331E1A",
          900: "#110A09",
        },

      },
    },
  },
  plugins: [
    require('@tailwindcss/forms'),
  ],
}
