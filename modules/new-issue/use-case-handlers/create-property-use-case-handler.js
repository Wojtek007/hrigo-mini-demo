export const createPropertyUseCaseHandler = ({ persistProperty }) => async ({ name }) => {
    const newProperty = {
        name
    }

    const { property, error } = await persistProperty({ property: newProperty })

    return {
        isSuccess: !!property,
        property,
        error
    }
}
