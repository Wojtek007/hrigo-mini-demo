export const createIssueUseCaseHandler = ({ persistIssue }) =>
    async ({
        title,
        description,
        propertyId,
        propertyVer,
        urgencyLevelId,
        urgencyLevelVer,
        requiresOwnerApproval,
        requiresExternalSpecialist,
    }) => {
        const newIssue = {
            title,
            description,
            propertyId,
            propertyVer,
            urgencyLevelId,
            urgencyLevelVer,
            requiresOwnerApproval,
            requiresExternalSpecialist,
        }

        const { issue, error } = await persistIssue({ issue: newIssue })

        return {
            isSuccess: !!issue,
            issue,
            error
        }
    }
