import TextInput from "/modules/form-engine/inputs/text-input";

const PropertyForm = ({ property, onInputChange, errorsMap }) => {
    return (
        <>
            <div className="md:grid md:grid-cols-3 md:gap-6">
                <div className="md:col-span-1">
                    <div className="px-4 sm:px-0">
                        <h3 className="text-lg font-medium leading-6 text-gray-900">Basic information</h3>
                        <p className="mt-1 text-sm text-gray-600">
                            Give it a name to identify this property in the future
                        </p>
                    </div>
                </div>
                <div className="mt-5 md:mt-0 md:col-span-2">
                    <div className="shadow sm:rounded-md ">
                        <div className="px-4 py-5 bg-white space-y-6 sm:p-6">
                            <TextInput
                                label="Name"
                                key="issue-name"
                                onChange={newValue => onInputChange({ newValue, key: 'name' })}
                                value={property.name}
                                error={errorsMap.name}
                                isObligatory
                            />
                        </div>
                    </div>
                </div>
            </div>
            <div className="hidden sm:block" aria-hidden="true">
                <div className="py-5">
                    <div className="border-t border-gray-200" />
                </div>
            </div>
        </>
    )
}

export default PropertyForm
