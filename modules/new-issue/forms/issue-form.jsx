import Dropdown from "/modules/form-engine/inputs/dropdown";
import TextAreaInput from "/modules/form-engine/inputs/text-area-input";
import TextInput from "/modules/form-engine/inputs/text-input";
import RadioInput from "/modules/form-engine/inputs/radio-input";
import BooleanInput from "/modules/form-engine/inputs/boolean-input";

const IssueForm = ({ issue, onInputChange, errorsMap, setIsSlideOverOpen, urgencyLevels, properties }) => {
    return (
        <>
            <div className="md:grid md:grid-cols-3 md:gap-6">
                <div className="md:col-span-1">
                    <div className="px-4 sm:px-0">
                        <h3 className="text-lg font-medium leading-6 text-gray-900">Describe</h3>
                        <p className="mt-1 text-sm text-gray-600">
                            Identify the issue based on information at your disposal
                        </p>
                    </div>
                </div>
                <div className="mt-5 md:mt-0 md:col-span-2">
                    <div className="shadow sm:rounded-md ">
                        <div className="px-4 py-5 bg-white space-y-6 sm:p-6">
                            <TextInput
                                label="Issue title"
                                key="issue-title"
                                value={issue.title}
                                onChange={newValue => onInputChange({ newValue, key: 'title' })}
                                error={errorsMap.title}
                                isObligatory
                            />
                            <TextAreaInput
                                label="Description"
                                desc="Brief description or message from a tenant."
                                key="issue-description"
                                value={issue.description}
                                onChange={newValue => onInputChange({ newValue, key: 'description' })}
                                error={errorsMap.note}
                            />
                            <Dropdown
                                label="Property"
                                key="issue-property"
                                value={issue.propertyId}
                                onChange={newValue => onInputChange({ newValue, key: 'propertyId' })}
                                error={errorsMap.propertyId}
                                isObligatory
                                options={properties.map(property => ({ value: property.id, label: property.name }))}
                                onCreate={() => setIsSlideOverOpen(true)}
                            />
                        </div>

                    </div>
                </div>
            </div>

            <div className="hidden sm:block" aria-hidden="true">
                <div className="py-5">
                    <div className="border-t border-gray-200" />
                </div>
            </div>

            <div className="mt-10 sm:mt-0">
                <div className="md:grid md:grid-cols-3 md:gap-6">
                    <div className="md:col-span-1">
                        <div className="px-4 sm:px-0">
                            <h3 className="text-lg font-medium leading-6 text-gray-900">Clasify</h3>
                            <p className="mt-1 text-sm text-gray-600">Evaluate the issue to the best of your knowledge</p>
                        </div>
                    </div>
                    <div className="mt-5 md:mt-0 md:col-span-2">
                        <div className="shadow overflow-hidden sm:rounded-md">
                            <div className="px-4 py-5 bg-white space-y-6 sm:p-6">
                                <RadioInput
                                    label="Urgency"
                                    desc="How urgently it needs to be solved?"
                                    key="issue-urgency"
                                    value={issue.urgencyLevelId}
                                    onChange={newValue => onInputChange({ newValue, key: 'urgencyLevelId' })}
                                    error={errorsMap.urgencyLevelId}
                                    options={urgencyLevels.map(urgencyLevel => ({ value: urgencyLevel.id, label: urgencyLevel.label }))}
                                    isObligatory
                                />
                                <BooleanInput
                                    label="External specialist"
                                    desc="Is external specialist required?"
                                    key="issue-external-specialist"
                                    value={issue.requiresExternalSpecialist}
                                    onChange={newValue => onInputChange({ newValue, key: 'requiresExternalSpecialist' })}
                                    error={errorsMap.requiresExternalSpecialist}
                                />
                                <BooleanInput
                                    label="Owner approval"
                                    desc="Is the issue fix costly enough to require owners approval?"
                                    key="issue-owner-approval"
                                    value={issue.requiresOwnerApproval}
                                    onChange={newValue => onInputChange({ newValue, key: 'requiresOwnerApproval' })}
                                    error={errorsMap.requiresOwnerApproval}
                                />

                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div className="hidden sm:block" aria-hidden="true">
                <div className="py-5">
                    <div className="border-t border-gray-200" />
                </div>
            </div>
        </>
    )
}

export default IssueForm
