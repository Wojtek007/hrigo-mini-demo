import { gql } from "@apollo/client";
import client from "/modules/apollo/apollo-client";

export const persistProperty = async ({ property }) => {
    const res = await client.mutate({
        mutation: gql`
            mutation PersistProperty($property: versionedProperties_insert_input = {}) {
                insertProperty(object: $property) {
                    id
                    ver
                    name
                }
            }
        `,
        variables: { property }
    })

    return {
        property: res.data.insertProperty,
        error: res?.data?.insertProperty ? null : 'Error in persistProperty infrastructure method',
    }
}