import { gql } from "@apollo/client";
import client from "/modules/apollo/apollo-client";

export const persistIssue = async ({ issue }) => {
    const res = await client.mutate({
        mutation: gql`
            mutation PersistIssue($issue: versionedIssues_insert_input = {}) {
                insertIssue(object: $issue) {
                    id
                    ver
                    title
                    description
                    createdAt
                    estimatedCost
                    requiresExternalSpecialist
                    requiresOwnerApproval
                    property {
                        id
                        ver
                        name
                    }
                    urgencyLevel {
                        id
                        ver
                        key
                        label
                    }
                }
            }
        `,
        variables: { issue }
    })

    return {
        issue: res.data.insertIssue,
        error: res?.data?.insertIssue ? null : 'Error in insertIssue infrastructure method',
    }
}