import { useState } from 'react';

import FormTemplate from '/modules/form-engine'
import { useForm } from '/modules/form-engine/hooks/use-form'

import SlideOver from "./components/slide-over-template";
import PropertyForm from './forms/property-form';
import IssueForm from './forms/issue-form';

import service from './service'

const CreateIssueForm = ({ urgencyLevels, properties }) => {
    const [isSlideOverOpen, setIsSlideOverOpen] = useState(false);

    const issueFormValidation = {
        title: title => !!title,
        propertyId: propertyId => !!propertyId && properties.find(p => p.id === propertyId),
        urgencyLevelId: urgencyLevelId => !!urgencyLevelId && urgencyLevels.find(p => p.id === urgencyLevelId),
    }
    const [submitIssueError, setSubmitIssueError] = useState();
    const [issue, onIssueChange, isIssueValid, issueErrorsMap] = useForm({}, issueFormValidation);
    const redirect = '/';

    const propertyFormValidation = {
        name: name => !!name,
    }
    const [submitPropertyError, setSubmitPropertyError] = useState();
    const [property, onPropertyChange, isPropertyValid, propertyErrorsMap, clearPropertyForm] = useForm({}, propertyFormValidation);

    const handleCreateIssue = async () => {
        const choosenProperty = properties.find(p => p.id === issue.propertyId);
        const choosenUrgencyLevel = urgencyLevels.find(u => u.id === issue.urgencyLevelId);

        const { isSuccess, error } = await service.createIssue({
            title: issue.title,
            description: issue.description,
            propertyId: issue.propertyId,
            propertyVer: choosenProperty.ver,
            urgencyLevelId: issue.urgencyLevelId,
            urgencyLevelVer: choosenUrgencyLevel.ver,
            requiresOwnerApproval: issue.requiresOwnerApproval,
            requiresExternalSpecialist: issue.requiresExternalSpecialist,
        });

        if (!isSuccess) {
            setSubmitIssueError(error)
        }

        return isSuccess
    }

    const handleCreateProperty = async () => {
        const { isSuccess, property: newProperty, error } = await service.createProperty({ name: property.name });

        if (isSuccess) {
            onIssueChange({ newValue: newProperty.id, key: 'propertyId' })
            clearPropertyForm()
        } else {
            setSubmitPropertyError(error)
        }

        return isSuccess
    }

    return (
        <>
            <FormTemplate
                title="Create new issue"
                onSubmit={handleCreateIssue}
                isValid={isIssueValid}
                errors={[submitIssueError].filter(Boolean)}
                redirect={redirect}
            >
                <IssueForm
                    issue={issue}
                    onInputChange={onIssueChange}
                    errorsMap={issueErrorsMap}
                    setIsSlideOverOpen={setIsSlideOverOpen}
                    urgencyLevels={urgencyLevels}
                    properties={properties}
                />
            </FormTemplate>
            <SlideOver
                isOpen={isSlideOverOpen}
                closePanel={() => setIsSlideOverOpen(false)}
            >
                <FormTemplate
                    title="Create new property"
                    onSubmit={handleCreateProperty}
                    isValid={isPropertyValid}
                    close={() => setIsSlideOverOpen(false)}
                    errors={[submitPropertyError].filter(Boolean)}
                >
                    <PropertyForm
                        property={property}
                        onInputChange={onPropertyChange}
                        errorsMap={propertyErrorsMap}
                    />
                </FormTemplate>
            </SlideOver>
        </>
    )
}

export default CreateIssueForm
