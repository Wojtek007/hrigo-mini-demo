// @App
import { createPropertyUseCaseHandler } from './use-case-handlers/create-property-use-case-handler';
import { createIssueUseCaseHandler } from './use-case-handlers/create-issue-use-case-handler';

// @Infra
import { persistProperty } from './infrastructure/persist-property'
import { persistIssue } from './infrastructure/persist-issue'

const service = ({
    createProperty: createPropertyUseCaseHandler({ persistProperty }),
    createIssue: createIssueUseCaseHandler({ persistIssue }),
})

export default service;
