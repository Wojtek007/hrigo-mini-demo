export default function RadioInput({
  label,
  key,
  onChange,
  value,
  desc,
  error,
  isObligatory,
  options
}) {
  return (
    <fieldset>
      <div>
        <legend className="text-base font-medium text-gray-900">{label}{isObligatory && ' *'}</legend>
        <p className="text-sm text-gray-500">{desc}</p>
      </div>
      <div className="mt-4 space-y-4">
        {options.map(option => (
          <div className="flex items-center">
            <input
              id={option.value}
              value={option.value}
              checked={option.value === value}
              onChange={() => onChange(option.value)}
              name="urgency-level"
              type="radio"
              className="focus:ring-copper-500 h-4 w-4 text-copper-600 border-gray-300"
            />
            <label htmlFor={option.value} className="ml-3 block text-sm font-medium text-gray-700">
              {option.label}
            </label>
          </div>
        ))}
      </div>
      {error && (
        <p className="mt-2 text-sm text-red-600" id={`${key}-error`}>
          {error}
        </p>
      )}
    </fieldset>
  );
}
