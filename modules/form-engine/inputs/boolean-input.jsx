export default function BooleanInput({
  label,
  key,
  onChange,
  value,
  desc,
  error,
  isObligatory
}) {
  return (
    <fieldset>
      <div>
        <legend className="text-base font-medium text-gray-900">{label}{isObligatory && ' *'}</legend>
        <p className="text-sm text-gray-500">{desc}</p>
      </div>
      <div className="mt-4 space-y-4">
        <div className="flex items-start">
          <div className="flex items-center h-5">
            <input
              id={key}
              name={key}
              type="checkbox"
              className="focus:ring-copper-500 h-4 w-4 text-copper-600 border-gray-300 rounded"
              checked={value === true}
              onChange={(e) => onChange(e.target.checked)}
            />
          </div>
          <div className="ml-3 text-sm">
            <label htmlFor={key} className="font-medium text-gray-700">
              Required
            </label>
          </div>
        </div>
      </div>
      {error && (
        <p className="mt-2 text-sm text-red-600" id={`${key}-error`}>
          {error}
        </p>
      )}
    </fieldset >
  );
}
