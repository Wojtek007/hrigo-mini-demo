import Select from "react-select";
import { PlusIcon } from "@heroicons/react/solid";

export default function Example({
  label,
  key,
  isObligatory,
  onChange,
  value,
  autoComplete,
  desc,
  options,
  error,
  onCreate
}) {
  const regularStyle =
    "block w-full shadow-sm sm:text-sm border border-gray-300 rounded-md focus-visible:outline-copper-500 focus-visible:border-copper-500 ";
  const errorStyle =
    "block w-full shadow-sm sm:text-sm border border-red-300 rounded-md focus-visible:outline-red-500 focus-visible:border-red-500 text-red-900 placeholder-red-300";

  return (
    <div>
      <label
        htmlFor={key}
        className="block text-sm font-medium text-gray-700"
      >
        {label}{isObligatory && ' *'}
      </label>
      <div className="flex shadow-none react-select-dropdown" >
        <Select
          name={key}
          autoComplete={autoComplete}
          isClearable
          className={error ? errorStyle : regularStyle}
          onChange={(option) => onChange(option ? option.value : null)}
          value={options.find((option) => option.value === value)}
          options={options}
          styles={{
            control: base => ({
              ...base,
              borderColor: error && 'rgb(239, 68, 68) !important',
              boxShadow: '0 !important',
              '&:focus-within': {
                boxShadow: `0 0 0 1px ${error ? 'rgb(239, 68, 68)' : 'rgb(153 91 77)'} !important`,
                borderColor: `${error ? 'rgb(239, 68, 68)' : 'rgb(153 91 77)'} !important`,
                border: '2 !important',
              }
            }),
          }}
        />
        <button
          type="button"
          onClick={onCreate}
          className="ml-2 inline-flex items-center p-2 border border-transparent rounded-full shadow-sm text-white bg-copper-600 hover:bg-copper-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-copper-500"
        >
          <PlusIcon className="h-5 w-5" aria-hidden="true" />
        </button>
      </div>
      {desc && <p className="mt-2 text-sm text-gray-500">{desc}</p>}
      {error && (
        <p className="mt-2 text-sm text-red-600" id={`${key}-error`}>
          {error}
        </p>
      )}
    </div>
  );
}
