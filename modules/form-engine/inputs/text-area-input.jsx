import { ExclamationCircleIcon } from "@heroicons/react/solid";

export default function TextAreaInput({
  label,
  key,
  placeholder,
  onChange,
  value,
  desc,
  error,
}) {
  const regularStyle =
    "mt-1 focus:ring-copper-500 focus:border-copper-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md";
  const errorStyle =
    "block w-full pr-10 border-red-300 text-red-900 placeholder-red-300 focus:outline-none focus:ring-red-500 focus:border-red-500 sm:text-sm rounded-md";
  return (
    <div className="col-span-6 sm:col-span-3">
      <label htmlFor={key} className="block text-sm font-medium text-gray-700">
        {label}
      </label>
      <div className="mt-1 relative rounded-md shadow-sm">
        <textarea
          id={key}
          name={key}
          rows={3}
          placeholder={placeholder}
          onChange={(e) => onChange(e.target.value)}
          value={value}
          className={error ? errorStyle : regularStyle}
          aria-invalid={!!error}
          aria-describedby={`${key}-error`}
        />
        {error && (
          <div className="absolute inset-y-0 right-0 pr-3 flex items-center pointer-events-none">
            <ExclamationCircleIcon
              className="h-5 w-5 text-red-500"
              aria-hidden="true"
            />
          </div>
        )}
      </div>
      {desc && <p className="mt-2 text-sm text-gray-500">{desc}</p>}
      {error && (
        <p className="mt-2 text-sm text-red-600" id={`${key}-error`}>
          {error}
        </p>
      )}
    </div>
  );
}
