import { useReducer } from 'react';
import { reducer, INPUT_CHANGE_ACTION, CLEAR_FORM_ACTION } from '../redux/reducer'

export const useForm = (initialValue, formValidation) => {
    const [formState, dispatch] = useReducer(reducer, initialValue ?? {});

    const handleOnInputChange = ({ newValue, key }) => dispatch({ type: INPUT_CHANGE_ACTION, newValue, key })
    const handleClearForm = () => dispatch({ type: CLEAR_FORM_ACTION })

    const errorsMap = Object.fromEntries(Object.entries(formValidation)
        .map(([key, isValid]) => ([key, formState[key] !== undefined && !isValid(formState[key]) && 'Required field'])
        ))

    const isValid = Object.entries(formValidation).every(([key, isValid]) => isValid(formState[key]))

    return [formState, handleOnInputChange, isValid, errorsMap, handleClearForm]
}