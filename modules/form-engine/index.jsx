import { useState } from "react";
import { useRouter } from "next/router";

import FormDesign from "./components/form-design";



const FormEngine = ({
  title,
  onSubmit,
  isValid,
  errors,
  redirect,
  close,
  children
}) => {
  const router = useRouter();
  const [loading, setLoading] = useState(false)
  const [isSuccesful, setIsSuccesful] = useState(false)

  const handleSubmit = async (e) => {
    e.preventDefault();
    setLoading(true)
    const isSuccessfulSubmit = await onSubmit()
    setLoading(false)

    if (isSuccessfulSubmit) {
      setIsSuccesful(true)

      if (close) {
        close();
      }
      if (redirect) {
        router.push(redirect);
      }
    }
  };

  const handleAbort = (e) => {
    e.preventDefault();

    if (close) {
      close();
    }
    if (redirect) {
      router.push(redirect);
    }
  };

  return (
    <FormDesign
      title={title}
      onAbort={handleAbort}
      onSubmit={handleSubmit}
      isSubmitDisabled={!isValid}
      isSubmitLoading={loading}
      isSubmitDone={isSuccesful}
      error={errors && errors.length > 0 && JSON.stringify(errors)}
      submitLabel="Create"
      submitSuccessLabel="Created!"
    >
      {children}
    </FormDesign>
  );
};

export default FormEngine;
