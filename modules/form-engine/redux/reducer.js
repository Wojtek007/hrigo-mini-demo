export const INPUT_CHANGE_ACTION = 'input-change';
export const CLEAR_FORM_ACTION = 'clear-form';

export const reducer = (state, action) => {
    switch (action.type) {
        case INPUT_CHANGE_ACTION:
            const { newValue, key } = action;
            return { ...state, [key]: newValue }
        case CLEAR_FORM_ACTION:
            return {}
        default:
            throw new Error();
    }
}