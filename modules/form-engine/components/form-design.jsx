import { XCircleIcon } from "@heroicons/react/solid";

export default function FormDesign({
    children,
    title = "Temp Title",
    onAbort,
    onSubmit,
    isSubmitDisabled,
    isSubmitLoading,
    isSubmitDone,
    error,
    submitLabel = "Create",
    submitSuccessLabel = "Created!",
    submitInProgress = "Loading..."
}) {
    return (
        <div className="pb-32 bg-gray-100">
            <header className="sticky top-0 pt-10 pb-3 bg-gray-100 z-10 backdrop-blur-sm bg-white/70 shadow-md">
                <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
                    <div className="md:flex md:items-center md:justify-between">
                        <div className="flex-1 min-w-0">
                            <h1 className="text-2xl font-bold leading-7 text-gray-900 sm:text-3xl sm:truncate">
                                {title}
                            </h1>
                        </div>
                        <div className="mt-4 flex md:mt-0 md:ml-4">
                            <button
                                type="button"
                                onClick={onAbort}
                                className="inline-flex items-center px-4 py-2 border border-gray-300 rounded-md shadow-sm text-sm font-medium text-gray-700 bg-white hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-copper-500"
                            >
                                Cancel
                            </button>
                            <button
                                type="button"
                                onClick={onSubmit}
                                disabled={isSubmitDisabled || isSubmitLoading || isSubmitDone}
                                className="ml-3 inline-flex items-center px-4 py-2 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-copper-600 hover:bg-copper-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-copper-500 disabled:opacity-50 disabled:bg-gray-500 disabled:text-gray-100"
                            >
                                {isSubmitLoading
                                    ? submitInProgress
                                    : isSubmitDone
                                        ? submitSuccessLabel
                                        : submitLabel}
                            </button>
                        </div>
                    </div>
                    {error && (
                        <div className="rounded-md bg-red-50 p-4 mt-3">
                            <div className="flex">
                                <div className="flex-shrink-0">
                                    <XCircleIcon
                                        className="h-5 w-5 text-red-400"
                                        aria-hidden="true"
                                    />
                                </div>
                                <div className="ml-3">
                                    <h3 className="text-sm font-medium text-red-800">{error}</h3>

                                </div>
                            </div>
                        </div>
                    )}
                </div>
            </header>
            <main>
                <div className="max-w-7xl mx-auto py-6 sm:px-6 lg:px-8">
                    {children}
                </div>
            </main>
        </div>
    );
}
