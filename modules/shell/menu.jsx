import Link from 'next/link'
import { PlusSmIcon } from '@heroicons/react/solid'

export default function MenuBar() {
    return (
        <nav className="shadow border-b border-gray-200 sticky top-0 backdrop-blur-sm bg-white/70">

            <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
                <div className="flex justify-between h-16">
                    <div className="flex">

                        <div className="flex-shrink-0 flex items-center">
                            <h1 className="flex items-center bg-white p-1 shadow-blur-edges rounded-lg text-2xl tracking-tight font-extrabold text-gray-900 sm:text-3xl md:text-4xl">
                                <span className="block inline">H</span>
                                <span className="block text-copper-600 inline">rigo</span>
                                <span className="ml-1 block text-xs inline leading-3 tracking-tight">
                                    <div>home</div>
                                    <div>issues</div>
                                    <div>edition</div>
                                </span>
                            </h1>
                        </div>
                    </div>
                    <div className="flex items-center">
                        <div className="flex-shrink-0">
                            <Link href="/issues/new">
                                <a>
                                    <button
                                        type="button"
                                        className="relative inline-flex items-center px-4 py-2 border border-transparent text-sm font-medium rounded-md text-white bg-copper-600 shadow-sm hover:bg-copper-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-copper-500"
                                    >
                                        <PlusSmIcon className="-ml-1 mr-2 h-5 w-5" aria-hidden="true" />
                                        <span>Add Issue</span>
                                    </button>
                                </a>
                            </Link>
                        </div>

                    </div>
                </div>
            </div>
        </nav>
    )
}
