import { StatusOfflineIcon, CloudIcon, DatabaseIcon, SwitchHorizontalIcon } from '@heroicons/react/outline'

import BannerDesign from './components/banner-design'

export default function Banner({
    isOnStaticData,
    hasNoStaticButWaitsForSub,
    isOnSubscribeData,
    isBackendOffline
}) {
    if (hasNoStaticButWaitsForSub) {
        const icon = (
            <span className="flex p-2 rounded-lg bg-yellow-800">
                <SwitchHorizontalIcon className="h-6 w-6 text-white" aria-hidden="true" />
            </span>
        )
        return <BannerDesign background="bg-yellow-600" icon={icon} message="No static data but trying to connect" />
    }
    if (isOnStaticData) {
        const icon = (
            <span className="flex p-2 rounded-lg bg-gray-800">
                <DatabaseIcon className="h-6 w-6 text-white" aria-hidden="true" />
            </span>
        )
        return <BannerDesign background="bg-gray-600" icon={icon} message="App is statically generated" />
    }
    if (isBackendOffline) {
        const icon = (
            <span className="flex p-2 rounded-lg bg-red-800">
                <StatusOfflineIcon className="h-6 w-6 text-white" aria-hidden="true" />
            </span>
        )
        return <BannerDesign background="bg-red-600" icon={icon} message="Server is offline" />
    }
    if (isOnSubscribeData) {
        const icon = (
            <span className="flex p-2 rounded-lg bg-copper-800">
                <CloudIcon className="h-6 w-6 text-white" aria-hidden="true" />
            </span>
        )
        return <BannerDesign background="bg-copper-600" icon={icon} message="App is on real-time data" />
    }

    return null
}
