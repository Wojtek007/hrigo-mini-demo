
function classNames(...classes) {
    return classes.filter(Boolean).join(" ");
}

export default function BannerDesign({ background, icon, message }) {
    return (
        <div className="fixed bottom-0 inset-x-0 pb-2 sm:pb-5">
            <div className="max-w-7xl mx-auto px-2 sm:px-6 lg:px-8">
                <div className={classNames(background, "p-2 rounded-lg shadow-lg sm:p-3")}>
                    <div className="flex items-center justify-between flex-wrap">
                        <div className="w-0 flex-1 flex items-center">
                            {icon}
                            <p className="ml-3 font-medium text-white truncate">
                                {message}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
