import { CalendarIcon, UserIcon, HomeIcon, BriefcaseIcon } from '@heroicons/react/solid'
import * as moment from "moment";

export default function IssueListElement({ issue }) {
    return (
        <li key={issue.id} className="bg-white shadow overflow-hidden px-2 py-2 sm:rounded-md hover:bg-gray-50">
            {/* <a href="#" className="block"> */}
            <div className="px-4 py-4 sm:px-6 grid grid-cols-2 sm:grid-cols-3">
                <div className="flex flex-col items-start sm:col-span-2">
                    <p className="text-lg font-bold text-copper-600 break-words">{issue.title}</p>

                    <p className="mt-4 flex items-center text-sm text-gray-500 break-words">
                        {issue.description}
                    </p>
                </div>
                <div className="ml-2 mt-2 flex flex-col  items-start">
                    <div className="flex-shrink-0 flex">
                        {(() => {
                            switch (issue.urgencyLevel.key) {
                                case "URGENT":
                                    return (
                                        <p className="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-red-100 text-red-800">
                                            {issue.urgencyLevel.label}
                                        </p>
                                    );
                                case "POSTPONED":
                                    return (
                                        <p className="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-blue-100 text-blue-800">
                                            {issue.urgencyLevel.label}
                                        </p>
                                    );
                                case "REFUSED":
                                    return (
                                        <p className="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-gray-100 text-gray-800">
                                            {issue.urgencyLevel.label}
                                        </p>
                                    );
                            }
                        })()}
                    </div>
                    <p className="mt-2 flex items-center text-sm text-gray-500 break-words">
                        <HomeIcon className="flex-shrink-0 mr-1.5 h-5 w-5 text-gray-400" aria-hidden="true" />
                        {issue.property.name}
                    </p>
                    <div className="mt-2 flex items-center text-sm text-gray-500">
                        <CalendarIcon className="flex-shrink-0 mr-1.5 h-5 w-5 text-gray-400" aria-hidden="true" />
                        <p>
                            Reported on <time dateTime={issue.createdAt}>{moment(issue.createdAt).format("L")}</time>
                        </p>
                    </div>
                    {issue.requiresOwnerApproval && <p className="mt-2 flex items-center text-sm text-gray-500">
                        <UserIcon className="flex-shrink-0 mr-1.5 h-5 w-5 text-gray-400" aria-hidden="true" />
                        Needs owner approval
                    </p>}
                    {issue.requiresExternalSpecialist && <p className="mt-2 flex items-center text-sm text-gray-500">
                        <BriefcaseIcon className="flex-shrink-0 mr-1.5 h-5 w-5 text-gray-400" aria-hidden="true" />
                        Needs external expert
                    </p>}
                </div>
            </div>
            {/* </a> */}
        </li>
    )
}
