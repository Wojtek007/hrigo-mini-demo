import IssueListElement from './components/issue';

export default function IssuesList({ issues }) {
    return (
        <div className="bg-gray-100 py-4 ">
            <div className="max-w-2xl mx-auto sm:px-6">
                <ul role="list" className="space-y-3 mb-4 ">
                    {issues.map((issue) => (
                        <IssueListElement key={issue.id} issue={issue} />
                    ))}
                </ul>
            </div>
        </div >
    )
}
