import { ApolloClient, InMemoryCache } from '@apollo/client';
import { split, HttpLink } from '@apollo/client';
import { getMainDefinition } from '@apollo/client/utilities';
import { WebSocketLink } from '@apollo/client/link/ws';

const httpLink = new HttpLink({
    uri: `https://${process.env.HASURA_URL}/v1/graphql`,
    credentials: "include",
    headers: {
        "x-hasura-admin-secret": process.env.HASURA_ADMIN_SECRET
    },
});

const wssLink = process.browser ? new WebSocketLink({ // if you instantiate in the server, the error will be thrown
    uri: `wss://${process.env.HASURA_URL}/v1/graphql`,
    options: {
        reconnect: true,
        connectionParams: {
            headers: {
                "x-hasura-admin-secret": process.env.HASURA_ADMIN_SECRET
            },
        }
    },
}) : null;

const splitLink = process.browser ? split(
    ({ query }) => {
        const definition = getMainDefinition(query);
        return (
            definition.kind === 'OperationDefinition' &&
            definition.operation === 'subscription'
        );
    },
    wssLink,
    httpLink,
) : httpLink;

const client = new ApolloClient({
    ssrMode: typeof window === 'undefined',
    link: splitLink,
    cache: new InMemoryCache(),
});

export default client;

