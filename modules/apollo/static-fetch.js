export async function staticFetch(query, { variables } = {}) {
    const res = await fetch(`https://${process.env.HASURA_URL}/v1/graphql`, {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
            // Authorization: `Bearer ${accessToken}`,
            "x-hasura-admin-secret": process.env.HASURA_ADMIN_SECRET,
        },
        body: JSON.stringify({
            query,
            variables,
        }),
    });

    const json = await res.json();
    if (json.errors) {
        console.error(json.errors);
        throw new Error("Failed to fetch API", json.errors);
    }
    return json.data;
}