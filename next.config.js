module.exports = {
  reactStrictMode: true,
  env: {
    HASURA_ADMIN_SECRET: process.env.HASURA_ADMIN_SECRET,
    HASURA_URL: process.env.HASURA_URL,
  },
}

