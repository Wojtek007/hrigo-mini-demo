import { useSubscription, gql } from "@apollo/client";
import { staticFetch } from "../../modules/apollo/static-fetch"
import NewIssueForm from "../../modules/new-issue";
import Banner from "/modules/banner";


const PROPERTIES_QUERY = ({ subscribe } = {}) => `
  ${subscribe ? "subscription" : 'query'} Properties${subscribe ? "Sub" : 'Query'} {
    properties {
      id
      ver
      name
    }
  }
`;


const URGENCY_LEVELS_QUERY = ({ subscribe } = {}) => `
  ${subscribe ? "subscription" : 'query'} UrgencyLevels${subscribe ? "Sub" : 'Query'} {
    urgencyLevels {
      id
      ver
      key
      label
    }
  }
`;

async function parallel() {
  const propertiesPromise = staticFetch(PROPERTIES_QUERY());
  const urgencyLevelsPromise = staticFetch(URGENCY_LEVELS_QUERY());
  return {
    propertiesData: await propertiesPromise,
    urgencyLevelsData: await urgencyLevelsPromise,
  }
}

export async function getStaticProps() {
  try {
    const { propertiesData: { properties }, urgencyLevelsData: { urgencyLevels } } = await parallel();

    return {
      props: { properties, urgencyLevels },
    };
  } catch (error) {
    console.error("/issues/new getStaticProps - error:", error.message)

    return {
      props: { staticError: error.message },
    };
  }
}

export default function Home({ urgencyLevels, properties: staticProperties, staticError }) {
  const { data, loading, error } = useSubscription(gql`${PROPERTIES_QUERY({ subscribe: true })}`);

  const properties = loading || error ? staticProperties : data.properties

  const isOnStaticData = !staticError && !data;
  const isOnSubscribeData = data && !error;
  const isBackendOffline = staticError

  return (
    <>
      {!staticError && <NewIssueForm urgencyLevels={urgencyLevels} properties={properties} />}
      <Banner
        isOnStaticData={isOnStaticData}
        isOnSubscribeData={isOnSubscribeData}
        isBackendOffline={isBackendOffline}
      />
    </>

  )
}
