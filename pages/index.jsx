import { useSubscription, gql } from "@apollo/client";
import IssuesList from "/modules/issues-list";
import MenuBar from "/modules/shell/menu";
import { staticFetch } from "/modules/apollo/static-fetch"
import Banner from "/modules/banner";


const ISSUES_QUERY = ({ subscribe } = {}) => `
  ${subscribe ? "subscription" : 'query'} Issues${subscribe ? "Sub" : 'Query'} {
    issues(order_by: {createdAt: desc}) {
      id
      ver
      title
      description
      createdAt
      estimatedCost
      requiresExternalSpecialist
      requiresOwnerApproval
      property {
        id
        ver
        name
      }
      urgencyLevel {
        id
        ver
        key
        label
      }
    }
  }
`;


export async function getStaticProps() {
  try {
    const { issues } = await staticFetch(ISSUES_QUERY());

    return {
      props: { issues },
    };
  } catch (error) {
    console.error("/index getStaticProps - error:", error.message)

    return {
      props: { staticError: error.message, issues: [] },
    };
  }
}

export default function Home({ issues: staticIssues, staticError }) {
  const { data, loading, error } = useSubscription(gql`${ISSUES_QUERY({ subscribe: true })}`);

  const issues = loading || error ? staticIssues : data.issues;

  const isOnStaticData = !staticError && !data;
  const hasNoStaticButWaitsForSub = staticError && loading;
  const isOnSubscribeData = data && !error;
  const isBackendOffline = staticError && error && !data;

  return (
    <>
      <MenuBar />
      <IssuesList issues={issues} />
      <Banner
        isOnStaticData={isOnStaticData}
        hasNoStaticButWaitsForSub={hasNoStaticButWaitsForSub}
        isOnSubscribeData={isOnSubscribeData}
        isBackendOffline={isBackendOffline}
      />
    </>
  )
}
