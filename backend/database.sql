
  CREATE SCHEMA versioned;



 
CREATE TABLE versioned.issues (
  id uuid DEFAULT gen_random_uuid() NOT NULL,
  ver integer NOT NULL,
  deleted boolean,
  created_at timestamp with time zone DEFAULT now(),
  created_by text,
  title text NOT NULL, 
description text , 
estimated_cost numeric , 
requires_owner_approval boolean , 
requires_external_specialist boolean , 
property_ver integer NOT NULL, 
      property_id uuid NOT NULL, 
urgency_level_ver integer NOT NULL, 
      urgency_level_id uuid NOT NULL
);

CREATE SEQUENCE versioned.issues_ver_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
    OWNED BY versioned.issues.ver;

ALTER TABLE ONLY versioned.issues ALTER COLUMN ver SET DEFAULT nextval('versioned.issues_ver_seq'::regclass);

ALTER TABLE ONLY versioned.issues
    ADD CONSTRAINT issues_pkey PRIMARY KEY (id, ver);


 
CREATE TABLE versioned.properties (
  id uuid DEFAULT gen_random_uuid() NOT NULL,
  ver integer NOT NULL,
  deleted boolean,
  created_at timestamp with time zone DEFAULT now(),
  created_by text,
  name text NOT NULL
);

CREATE SEQUENCE versioned.properties_ver_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
    OWNED BY versioned.properties.ver;

ALTER TABLE ONLY versioned.properties ALTER COLUMN ver SET DEFAULT nextval('versioned.properties_ver_seq'::regclass);

ALTER TABLE ONLY versioned.properties
    ADD CONSTRAINT properties_pkey PRIMARY KEY (id, ver);


 
CREATE TABLE versioned.urgency_levels (
  id uuid DEFAULT gen_random_uuid() NOT NULL,
  ver integer NOT NULL,
  deleted boolean,
  created_at timestamp with time zone DEFAULT now(),
  created_by text,
  key text NOT NULL, 
label text NOT NULL
);

CREATE SEQUENCE versioned.urgency_levels_ver_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
    OWNED BY versioned.urgency_levels.ver;

ALTER TABLE ONLY versioned.urgency_levels ALTER COLUMN ver SET DEFAULT nextval('versioned.urgency_levels_ver_seq'::regclass);

ALTER TABLE ONLY versioned.urgency_levels
    ADD CONSTRAINT urgency_levels_pkey PRIMARY KEY (id, ver);


 

CREATE OR REPLACE VIEW public.issues as
    WITH last_version AS (
        SELECT
           row1.ver,
           row1.id,
           row1.created_at AS updated_at,
           row1.created_by AS updated_by,
           row1.title, row1.description, row1.estimated_cost, row1.requires_owner_approval, row1.requires_external_specialist, row1.property_id, row1.property_ver, row1.urgency_level_id, row1.urgency_level_ver
        FROM versioned.issues row1
        LEFT JOIN versioned.issues row2 ON row1.ver < row2.ver AND row1.id = row2.id
        WHERE row2.ver IS NULL AND row1.deleted IS NULL
    ), first_version AS (
        SELECT
            row1.id,
            row1.created_at,
            row1.created_by
        FROM versioned.issues row1
        LEFT JOIN versioned.issues row2 ON row1.ver > row2.ver AND row1.id = row2.id
        WHERE row2.ver IS NULL
    )
    SELECT lv.id,
           lv.ver,
           lv.updated_at,
           lv.updated_by,
           fv.created_at,
           fv.created_by,
           lv.title, lv.description, lv.estimated_cost, lv.requires_owner_approval, lv.requires_external_specialist, lv.property_id, lv.property_ver, lv.urgency_level_id, lv.urgency_level_ver
    FROM last_version lv
    LEFT JOIN first_version fv ON fv.id = lv.id;

 

CREATE OR REPLACE VIEW public.properties as
    WITH last_version AS (
        SELECT
           row1.ver,
           row1.id,
           row1.created_at AS updated_at,
           row1.created_by AS updated_by,
           row1.name
        FROM versioned.properties row1
        LEFT JOIN versioned.properties row2 ON row1.ver < row2.ver AND row1.id = row2.id
        WHERE row2.ver IS NULL AND row1.deleted IS NULL
    ), first_version AS (
        SELECT
            row1.id,
            row1.created_at,
            row1.created_by
        FROM versioned.properties row1
        LEFT JOIN versioned.properties row2 ON row1.ver > row2.ver AND row1.id = row2.id
        WHERE row2.ver IS NULL
    )
    SELECT lv.id,
           lv.ver,
           lv.updated_at,
           lv.updated_by,
           fv.created_at,
           fv.created_by,
           lv.name
    FROM last_version lv
    LEFT JOIN first_version fv ON fv.id = lv.id;

 

CREATE OR REPLACE VIEW public.urgency_levels as
    WITH last_version AS (
        SELECT
           row1.ver,
           row1.id,
           row1.created_at AS updated_at,
           row1.created_by AS updated_by,
           row1.key, row1.label
        FROM versioned.urgency_levels row1
        LEFT JOIN versioned.urgency_levels row2 ON row1.ver < row2.ver AND row1.id = row2.id
        WHERE row2.ver IS NULL AND row1.deleted IS NULL
    ), first_version AS (
        SELECT
            row1.id,
            row1.created_at,
            row1.created_by
        FROM versioned.urgency_levels row1
        LEFT JOIN versioned.urgency_levels row2 ON row1.ver > row2.ver AND row1.id = row2.id
        WHERE row2.ver IS NULL
    )
    SELECT lv.id,
           lv.ver,
           lv.updated_at,
           lv.updated_by,
           fv.created_at,
           fv.created_by,
           lv.key, lv.label
    FROM last_version lv
    LEFT JOIN first_version fv ON fv.id = lv.id;

 


  ALTER TABLE ONLY versioned.issues
      ADD CONSTRAINT issues_property_properties FOREIGN KEY (property_ver, property_id) REFERENCES versioned.properties(ver, id) ON UPDATE RESTRICT ON DELETE RESTRICT;
   
  ALTER TABLE ONLY versioned.issues
      ADD CONSTRAINT issues_urgency_level_urgency_levels FOREIGN KEY (urgency_level_ver, urgency_level_id) REFERENCES versioned.urgency_levels(ver, id) ON UPDATE RESTRICT ON DELETE RESTRICT;
  
 

    INSERT INTO versioned.urgency_levels (label,key) VALUES ('Urgent','URGENT');
    INSERT INTO versioned.urgency_levels (label,key) VALUES ('Postponed','POSTPONED');
    INSERT INTO versioned.urgency_levels (label,key) VALUES ('Refused','REFUSED');
